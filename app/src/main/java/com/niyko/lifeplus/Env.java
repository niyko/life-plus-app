package com.niyko.lifeplus;

import java.util.HashMap;

public class Env {
    public static HashMap<String, String> config = new HashMap<String, String>();

    public HashMap<String, String> getConfig(){
        config.put("server-url", "https://lifeplus.niyko.com/");
        if(BuildConfig.DEBUG) config.put("server-url", "http://192.168.29.40/NIYKO/Web/Life-Plus/public/");
        return config;
    }
}