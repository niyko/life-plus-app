package com.niyko.lifeplus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new android.os.Handler().postDelayed(
                () -> runOnUiThread(() -> {
                    Intent myIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(myIntent);
                    finish();
                }),
                2000);
    }
}