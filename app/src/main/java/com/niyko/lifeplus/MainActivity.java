package com.niyko.lifeplus;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private WebView appWebView;
    private LinearLayout noInternetLayout;
    private RelativeLayout pageLoadingLayout;

    public ValueCallback<Uri[]> uploadMessage;
    public static final int REQUEST_SELECT_FILE = 100;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appWebView = findViewById(R.id.appWebView);
        pageLoadingLayout = findViewById(R.id.pageLoadingLayout);
        noInternetLayout = findViewById(R.id.noInternetLayout);

        appWebView.getSettings().setJavaScriptEnabled(true);
        appWebView.getSettings().setLoadWithOverviewMode(true);
        appWebView.getSettings().setLoadWithOverviewMode(true);
        appWebView.getSettings().setUseWideViewPort(true);
        appWebView.getSettings().setDatabaseEnabled(true);
        appWebView.getSettings().setGeolocationEnabled(true);
        appWebView.setBackgroundColor(0);
        appWebView.getSettings().setDomStorageEnabled(true);
        appWebView.getSettings().setAllowContentAccess(true);
        appWebView.getSettings().setAllowFileAccess(true);
        appWebView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                if(consoleMessage.message().equals("--app-exit")) finish();
                return super.onConsoleMessage(consoleMessage);
            }

            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                if (uploadMessage != null) {
                    uploadMessage.onReceiveValue(null);
                    uploadMessage = null;
                }
                uploadMessage = filePathCallback;
                Intent intent = fileChooserParams.createIntent();
                intent.setType("image/*");
                try {
                    startActivityForResult(intent, REQUEST_SELECT_FILE);
                }
                catch (ActivityNotFoundException e) {
                    uploadMessage = null;
                    Toast.makeText(MainActivity.this, "Cannot Open File Chooser", Toast.LENGTH_LONG).show();
                    return false;
                }
                return true;
            }
        });
        appWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                noInternetLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                showLoading(url);
                super.onLoadResource(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(url.contains("niyko.com/privacy")){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                    return true;
                }
                else if(url.contains("tel:")) {
                    Intent intentCall = new Intent();
                    intentCall.setAction(Intent.ACTION_DIAL);
                    intentCall.setData(Uri.parse(url));
                    startActivity(intentCall);
                    return true;
                }
                else {
                    return false;
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                showLoading(url);
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if(url.equals(new Env().getConfig().get("server-url")) || url.contains("/signin")){
                    appWebView.clearHistory();
                }
                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    pageLoadingLayout.animate().alpha(0f).setDuration(300).start();
                    pageLoadingLayout.setClickable(false);
                }, 300);
            }
        });

        appWebView.loadUrl(new Env().getConfig().get("server-url"));
    }

    public void showLoading(String url){
        if(!url.contains("favicon") && pageLoadingLayout.getAlpha()==0f) {
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                pageLoadingLayout.animate().alpha(1f).setDuration(100).start();
                pageLoadingLayout.setClickable(true);
            }, 200);
        }
    }

    public void afterInternetBack(View view){
        pageLoadingLayout.setVisibility(View.VISIBLE);
        noInternetLayout.setVisibility(View.INVISIBLE);
        appWebView.reload();
    }

    @Override
    public void onBackPressed() {
        if(appWebView.canGoBack()) appWebView.goBack();
        else finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == REQUEST_SELECT_FILE) {
            if (uploadMessage == null) return;
            uploadMessage.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(resultCode, intent));
            uploadMessage = null;
        }
    }
}